<?php

class Args {
    private $params;

    function __construct($schema, $params)  {
        $this->validateSchema($schema);
        $this->validateParams($schema, $params);
        $this->params = [substr($params[0], 1, 1) => $params[1]];
    }

    public function getParam($param){
        return $this->params[$param];
    } 

    private function validateSchema($schema) {
        $schemaElements = explode(",", $schema);
        $this->validateDuplicates($schemaElements);
        foreach($schemaElements as $element) {
            $this->validateTypeElementSchema($element);
            $this->validateLengthSchema($element);
        }
    }

    private function validateDuplicates($schemaElements) {
        $schemaLength = count($schemaElements);
        $uniqueSchemaLength = count(array_unique($schemaElements));

        if($schemaLength != $uniqueSchemaLength) {
            throw new \Exception("Repeat schema elements");
        }
    }
    
    private function validateTypeElementSchema($schemaElement) {
        $elementId = $schemaElement[0];
        $type = $schemaElement[1];

        if($type != "#") {
            throw new \Exception("Not valid type");
        }

        if(!ctype_lower($elementId)){
            throw new \Exception("Element id not valid");
        }
    }

    private function validateLengthSchema($schemaElement) {
        if(strlen($schemaElement) != 2) {
            throw new \Exception("Not valid length");
        }
    }

    private function validateParams($schema, $params) {
        $this->validateEmptyParams($params);
        $this->haveEvenParams($params);
        $this->validateEvenParams($params);
        $this->validateLengthSchemaAndParams($schema, $params);
        $this->validateSchemaAndParams($schema, $params);
    }

    private function validateEmptyParams($params) {
        if(empty($params)) {
            throw new \Exception("Params empty");
        }
    }

    private function haveEvenParams($params) {
        if((count($params) % 2) != 0) {
            throw new \Exception("Params are not even");
        }
    }

    private function validateEvenParams($params) {
        if(!ctype_digit($params[1])) {
            throw new \Exception("Even param is not an integer");
        }
    }

    private function validateLengthSchemaAndParams($schema, $params) {
        $schemaElements = explode(",", $schema);

        if(count($schemaElements)*2 != count($params)) {
            throw new \Exception("Schema number and params number are incompatibles");
        }
    }

    private function validateSchemaAndParams($schema, $params) {
        $schemaElements = explode(",", $schema);
        
    }
}
