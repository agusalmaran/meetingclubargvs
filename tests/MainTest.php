<?php

use PHPUnit\Framework\TestCase;

require_once ('src/Main.php');
 
class MainTest extends TestCase
{
    public function testArgsValidateSchemaWithIntegers () {
        $this->assertArgsRaiseAnException("d#", ["-d", "1", "-f"]);
        $this->assertArgsRaiseAnException("d.", ["-d", "1"]);
        $this->assertArgsRaiseAnException("d#", ["-d", "a"]);
        $this->assertArgsNotRaiseAnException("d#", ["-d", "1"]);
        $this->assertArgsNotRaiseAnException("d#,p#", ["-d", "1", "-p", "2"]);
        $this->assertArgsRaiseAnException("d#,d#", ["-d", "1", "-d", "2"]);
        $this->assertArgsRaiseAnException("1#", ["-1", "1"]);
        $this->assertArgsRaiseAnException("A#", ["-A", "1"]);
        $this->assertArgsRaiseAnException("d#", ["-d", "2", "-f", "3"]);
        $this->assertArgsRaiseAnException("d#,f#", ["-d", "1"]);
        $this->assertArgsNotRaiseAnException("d#,p#", ["-p", "1", "-d", "2"]);
    }

    public function testArgumentMustBeInTheSchema() {
        $this->assertArgsRaiseAnException("d#", []);
        //$this->assertArgsRaiseAnException("d#", ["-f", "1"]);
    }

    public function assertArgsRaiseAnException($schema, $argv) {
        try {
            $args = new Args($schema, $argv);
        } catch (\Exception $e) {
            return $this->assertTrue(true);
        }
        return $this->assertTrue(false);
    }

    public function assertArgsNotRaiseAnException($schema, $argv) {
        try {
            $args = new Args($schema, $argv);
        } catch (\Exception $e) {
            return $this->assertTrue(false);
        }            
        return $this->assertTrue(true);

    }

    public function testArgsShouldProvideIntegerParam(){
        $args = new Args("d#", ["-d", "1"]);

        $this->assertTrue(1 == $args->getParam("d"));

        $args = new Args("p#", ["-p", "2"]);
        $this->assertTrue(2 == $args->getParam("p"));
    } 
}